import React from "react";
import WeatherForecast from "./components/WeatherForecast";
import Scale from './components/Scale';

import {Container} from 'react-bootstrap';

export default function App() {
  return (
    <Container fluid>
      <header className="d-flex justify-content-between align-items-center mt-5 mb-5">
        <h1>Weather forecast</h1>
        <Scale />
      </header>
      <WeatherForecast />
    </Container>
  );
}
