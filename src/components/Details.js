import React from "react";
import PropTypes from 'prop-types';
import styled from 'styled-components';

const StyledList = styled.ul`
  list-style: none;
  padding: 0;
  width: 100%;
  margin-top: 2rem;
  
  li {
    padding: .3rem 0;
    border-top: 1px solid #e6e6e6;
    font-size: 13px;
    color: #909090;
    
    span {
      color: #000;
      font-weight: bold;
    }
  }
`;

const Details = props => {
  return <StyledList>
    <li>Max UV Index: <span className="float-right">{props.details.uv_index}</span></li>
    <li>Wind: <span className="float-right">{props.details.wind}</span></li>
    <li>Humidity <span className="float-right">{props.details.humidity}</span></li>
  </StyledList>;
};

Details.propTypes = {details: PropTypes.object};

export default Details;
