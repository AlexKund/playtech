import React, {useState, useEffect} from "react";
import {connect} from "react-redux";
import {toggleInfo} from "../redux/actions";
import styled from 'styled-components';

import {library} from '@fortawesome/fontawesome-svg-core'
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {faCloudRain, faSun, faCloudSun} from '@fortawesome/free-solid-svg-icons';

import Day from './Day';
import Details from './Details';
import {fetchWeek} from '../helpers';


const FontAwesomeIconStyled = styled(FontAwesomeIcon)`
  font-size: 105px;
  color: #e8e8e8;
  margin: 1rem;
`;

const Temperature = styled.div`
  font-size: 22px;
  font-weight: bold;
`;

library.add(faCloudRain, faSun, faCloudSun);

const WeatherForecast = ({infoId, toggleInfo, scaleId}) => {
  const [weekForecast, setWeekForecast] = useState([]);

  useEffect(() => {
    fetchWeek().then(res => {
      setWeekForecast(res.weekForecast)
    });
  }, []);

  const handleOnClickDay = e => toggleInfo(e.currentTarget.id);

  return <div className='d-flex flex-column flex-lg-row'>
    {weekForecast.map((e, i) => <Day key={i} title={e.day}>
      <FontAwesomeIconStyled icon={["fas", e.weather]} id={i} onClick={handleOnClickDay}/>
      <Temperature>{e.temperatures[scaleId]}</Temperature>
      {infoId.toString() === i.toString() && <Details details={e.details}/>}
    </Day>)}
  </div>;
};

const mapStateToProps = state => {
  return {infoId: state.infoId, scaleId: state.scaleId};
};

export default connect(
  mapStateToProps,
  {toggleInfo}
)(WeatherForecast);

