import React from "react";
import styled from 'styled-components';
import {Card} from 'react-bootstrap';

const StyledCard = styled(Card)`
  height: 30vh;
  flex-basis: 150px;
  min-height: 385px;
  transition: all .7s;
  
  &:hover {
    border: 1px solid #000;
    box-shadow: 0px 2px 13px #00000038;
  }
`;

const DayTitle = styled.div`
  color: #8a8a8a;
  font-size: 15px;
  margin-bottom: 22px;
`;


const Day = props => {
    return <StyledCard className="flex-grow-1 m-1 flex-shrink-0">
              <Card.Body className="d-flex align-items-center flex-column">
                <DayTitle className="w-100">{props.title}</DayTitle>
                {props.children}
              </Card.Body>
          </StyledCard>;
};

export default Day;
