import React, {useRef, useEffect} from "react";
import {connect} from "react-redux";
import {toggleScale} from "../redux/actions";
import styled from 'styled-components';

const StyledSelect= styled.select`
  width: 100%;
  max-width: 100px;
`;

const Scale = ({toggleScale}) => {
  let selectEl = useRef();

  useEffect(() =>{
    toggleScale(selectEl.current.value)
  }, []);

  const handleChangeScale = e => toggleScale(e.target.value);

  return <StyledSelect name="scale" id="scale" className="form-control w-25" ref={selectEl} onChange={handleChangeScale}>
    <option value="c">Celsius scale</option>
    <option value="f">Fahrenheit scale</option>
  </StyledSelect>;
};

export default connect(
  null,
  {toggleScale}
)(Scale);

