import {TOGGLE_INFO, TOGGLE_SCALE} from "./actionTypes";

export const toggleInfo = id => {
  return ({
    type: TOGGLE_INFO,
    payload: { id }
  });
};

export const toggleScale = id => {
  return ({
    type: TOGGLE_SCALE,
    payload: { id }
  });
};
