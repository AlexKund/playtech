import {TOGGLE_SCALE} from "../actionTypes";

const initialState = '';

const scaleId = (state = initialState, action) => {
  switch (action.type) {
    case TOGGLE_SCALE: {
      return action.payload.id;
    }
    default: {
      return state;
    }
  }
};

export default scaleId;