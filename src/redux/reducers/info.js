import {TOGGLE_INFO} from "../actionTypes";

const initialState = '';

const infoId = (state = initialState, action) => {
  switch (action.type) {
    case TOGGLE_INFO: {
      return action.payload.id;
    }
    default: {
      return state;
    }
  }
};

export default infoId;