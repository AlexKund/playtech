import {combineReducers} from "redux";
import scaleId from "./scale";
import infoId from "./info";

export default combineReducers({infoId, scaleId});
