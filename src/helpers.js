import axios from "axios";

export const fetchWeek = async () => {
  try {
    let response = await axios({
      method: "GET",
      url: '../content.json',
    });
    if (response.status === 200) {
      return response.data
    }
  } catch (e) {
    console.log("Error occurred")
  }
};